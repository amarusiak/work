﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Work.Entities;
using Work.DataLayer;

namespace Work.WebUI.MVC5.Controllers
{
    public class ResumeController : Controller
    {
        private WorkModelContext db = new WorkModelContext();

        // GET: /Resume/
        public ActionResult Index()
        {
            var resumes = db.Resumes.Include(r => r.Applicant).Include(r => r.EconomySector);
            return View(resumes.ToList());
        }

        // GET: /Resume/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Resume resume = db.Resumes.Find(id);
            if (resume == null)
            {
                return HttpNotFound();
            }
            return View(resume);
        }

        // GET: /Resume/Create
        public ActionResult Create()
        {
            ViewBag.ApplicantId = new SelectList(db.Applicants, "Id", "FirstName");
            ViewBag.EconomySectorId = new SelectList(db.EconomySectors, "Id", "Name");
            return View();
        }

        // POST: /Resume/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,ApplicantId,EconomySectorId,City,TypeOfJobId,Salary,Description,TypeOfJob")] Resume resume)
        {
            if (ModelState.IsValid)
            {
                db.Resumes.Add(resume);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ApplicantId = new SelectList(db.Applicants, "Id", "FirstName", resume.ApplicantId);
            ViewBag.EconomySectorId = new SelectList(db.EconomySectors, "Id", "Name", resume.EconomySectorId);
            return View(resume);
        }

        // GET: /Resume/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Resume resume = db.Resumes.Find(id);
            if (resume == null)
            {
                return HttpNotFound();
            }
            ViewBag.ApplicantId = new SelectList(db.Applicants, "Id", "FirstName", resume.ApplicantId);
            ViewBag.EconomySectorId = new SelectList(db.EconomySectors, "Id", "Name", resume.EconomySectorId);
            return View(resume);
        }

        // POST: /Resume/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,ApplicantId,EconomySectorId,City,TypeOfJobId,Salary,Description,TypeOfJob")] Resume resume)
        {
            if (ModelState.IsValid)
            {
                db.Entry(resume).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ApplicantId = new SelectList(db.Applicants, "Id", "FirstName", resume.ApplicantId);
            ViewBag.EconomySectorId = new SelectList(db.EconomySectors, "Id", "Name", resume.EconomySectorId);
            return View(resume);
        }

        // GET: /Resume/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Resume resume = db.Resumes.Find(id);
            if (resume == null)
            {
                return HttpNotFound();
            }
            return View(resume);
        }

        // POST: /Resume/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Resume resume = db.Resumes.Find(id);
            db.Resumes.Remove(resume);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
