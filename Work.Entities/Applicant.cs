﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Work.Entities
{
  public partial class Applicant
  {
    public Applicant()
    {
      this.Resumes = new HashSet<Resume>();
    }

    //[Key, HiddenInput(DisplayValue = false)]
    public int Id { get; set; }

    [Required(ErrorMessage = "Please enter a FirstName")]
    public string FirstName { get; set; }

    [Required(ErrorMessage = "Please enter a LastName")]
    public string LastName { get; set; }

    [Required(ErrorMessage = "Please enter DateOfBirth")]
    public Nullable<System.DateTime> DateOfBirth { get; set; }

    [Required(ErrorMessage = "Please enter a City")]
    public string City { get; set; }

    //public string PhoneNumber { get; set; }

    [Required(ErrorMessage = "Please enter an EMail")]
    public string EMail { get; set; }

    public int? UserId { get; set; }        //  FK
    public virtual User User { get; set; }
    //public int LoginId { get; set; }        //  FK
    //public string Password { get; set; }    //  FK

    public virtual ICollection<Resume> Resumes { get; set; }
  }
}

