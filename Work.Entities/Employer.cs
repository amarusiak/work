﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Work.Entities
{
  public partial class Employer
  {
    public Employer()
    {
      this.Vacancys = new HashSet<Vacancy>();
    }

    public int Id { get; set; }

    [Required(ErrorMessage = "Please enter a Name")]
    public string Name { get; set; }

    [Required(ErrorMessage = "Please enter a City")]
    public string City { get; set; }

    //public string PhoneNumber { get; set; }

    [Required(ErrorMessage = "Please enter a EMail")]
    public string EMail { get; set; }

    [DataType(DataType.MultilineText)]
    [Required(ErrorMessage = "Please enter a Description")]
    public string Description { get; set; }

    public int? UserId { get; set; }        //  FK
    public virtual User User { get; set; }
    //public int LoginId { get; set; }        //  FK
    //public string Password { get; set; }    //  FK

    public virtual ICollection<Vacancy> Vacancys { get; set; }
  }
}