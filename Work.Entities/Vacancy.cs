﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Work.Entities
{
  public partial class Vacancy
  {
    public int Id { get; set; }

    [Required]
    //[ForeignKey("Employer")]
    public int EmployerId { get; set; }         //  FK

    public string Position { get; set; }
    public string City { get; set; }

    //[ForeignKey("EconomySector")]
    public int EconomySectorId { get; set; }       //  FK

    //[ForeignKey("TypeOfJob")]
    public TypeOfJob TypeOfJobId { get; set; }        //  FK

    public int YearsOfExperience { get; set; }

    //[ForeignKey("TypeOfEducation")]
    public TypeOfEducation TypeOfEducationId { get; set; }  //  FK

    [Required(ErrorMessage = "Please enter a Salary")]
    public int Salary { get; set; }

    public string Description { get; set; }

    public virtual Employer Employer { get; set; }
    public virtual EconomySector EconomySector { get; set; }
    //public virtual TypeOfJob TypeOfJob { get; set; }
    //public virtual TypeOfEducation TypeOfEducation { get; set; }
  }
}

