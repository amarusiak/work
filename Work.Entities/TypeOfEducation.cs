﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Work.Entities
{
  public enum TypeOfEducation
  {
    Secondary = 1,
    Bachelor = 2,
    Master = 3
  }
}
