﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Work.Entities
{
  public partial class EconomySector
  {
    public EconomySector()
    {
      this.Resumes = new HashSet<Resume>();
      this.Vacancys = new HashSet<Vacancy>();
    }

    //[Key, HiddenInput(DisplayValue = false)]
    public int Id { get; set; }

    [Required(ErrorMessage = "Please enter an EconomySector Name")]
    public string Name { get; set; }

    public virtual ICollection<Resume> Resumes { get; set; }
    public virtual ICollection<Vacancy> Vacancys { get; set; }

  }
}
