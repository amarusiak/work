﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Work.Entities
{
  public partial class JobRecord
  {
    [Key, HiddenInput(DisplayValue = false)]
    public int Id { get; set; }

    [ForeignKey("Resume")]
    public int ResumeId { get; set; }         //  FK

    [Required(ErrorMessage = "Please enter a Company")]
    public string Company { get; set; }

    [Required(ErrorMessage = "Please enter a City")]
    public string City { get; set; }

    [Required(ErrorMessage = "Please enter a Position")]
    public string Position { get; set; }

    public Nullable<System.DateTime> FromDate { get; set; }
    public Nullable<System.DateTime> ToDate { get; set; }

    [DataType(DataType.MultilineText)]
    //[Required(ErrorMessage = "Please enter a description")]
    public string Description { get; set; }

    public virtual Resume Resume { get; set; }
  }
}
