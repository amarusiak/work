﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Work.Entities
{
  public partial class Resume
  {
    public Resume()
    {
      this.EducationRecords = new HashSet<EducationRecord>();
      this.JobRecords = new HashSet<JobRecord>();
    }

    //[Key, HiddenInput(DisplayValue = false)]
    public int Id { get; set; }

    //[ForeignKey("Applicant")]
    public int ApplicantId { get; set; }       //  FK

    //[ForeignKey("EconomySector")]
    public int EconomySectorId { get; set; }     //  FK

    [Required(ErrorMessage = "Please enter a City")]
    public string City { get; set; }

    //[ForeignKey("TypeOfJob")]
    public TypeOfJob TypeOfJobId { get; set; }      //  FK

    [Required(ErrorMessage = "Please enter a Salary")]
    public int Salary { get; set; }

    [Required(ErrorMessage = "Please enter a Description")]
    public string Description { get; set; }

    public virtual ICollection<EducationRecord> EducationRecords { get; set; }
    public virtual EconomySector EconomySector { get; set; }
    public virtual ICollection<JobRecord> JobRecords { get; set; }
    public virtual Applicant Applicant { get; set; }
    public virtual TypeOfJob TypeOfJob { get; set; }
  }
}
