﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Work.Entities
{
  public enum TypeOfJob
  {
    Remote = 1,
    PartTime = 2,
    FullTime = 3
  }
}
