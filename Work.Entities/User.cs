﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Work.Entities
{
  public partial class User
  {
    //[Key, HiddenInput(DisplayValue = false)]
    public int Id { get; set; }

    [Required(ErrorMessage = "Please enter a Login")]
    public string Login { get; set; }

    [Required(ErrorMessage = "Please enter a Password")]
    public string Password { get; set; }

  }
}
