﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Configuration;
using System.Data.SqlClient;
using Work.Entities;
using Work.DataLayer;
//using System.Data.Entity;

//namespace Work.Repositories.SqlRepositories
namespace Work.Repositories
{
  public class SqlEmployerRepository : IEmployerRepository
  {
    static string connectionString = ConfigurationManager.ConnectionStrings["ConnectToSQL"].ConnectionString;

    static string queryGetAll = "SELECT [Id],[Name],[City],[EMail],[Description] FROM [dbo].[Employers]";
    static string queryGetAllId = "SELECT [Id],[Name],[City],[EMail],[Description] FROM [dbo].[Employers] WHERE [Id] = @Id";
    //static string maxIdSelect = "SELECT MAX(Id) FROM [dbo].[Employers] GROUP BY ID";
    static string queryInsert = "INSERT INTO [dbo].[Employers] ([Name],[City],[EMail],[Description]) VALUES (@Name,@City,@EMail,@Description); SELECT CAST(SCOPE_IDENTITY() AS INT);";
    static string queryRemove = "DELETE FROM [dbo].[Employers] WHERE [Id] = @Id";

    static string queryUpdate =
      "UPDATE [dbo].[Employers] SET [FirstName]=@FirstName,[LastName]=@LastName,[DateOfBirth]=@DateOfBirth,[City]=@City,[EMail]=@EMail WHERE [Id] = @Id";

    public List<Employer> GetAll()
    {
      using (SqlConnection connection = new SqlConnection(connectionString))
      {
        connection.Open();

        using (SqlCommand command = new SqlCommand(queryGetAll, connection))
        {
          using (SqlDataReader reader = command.ExecuteReader())
          {
            List<Employer> employers = new List<Employer>();

            while (reader.Read())
            {
              employers.Add(new Employer()
              {
                Id            = (int)reader["id"],
                Name          = (string)reader["Name"],
                City          = (string)reader["City"],
                EMail         = (string)reader["EMail"],
                Description   = (string)reader["Description"]
              });
            }
            return employers;
          }
        }
      }
    }
    public Employer Get(int Id)
    {
      using (SqlConnection connection = new SqlConnection(connectionString))
      {
        connection.Open();

        using (SqlCommand command = new SqlCommand(queryGetAllId, connection))
        {
          command.Parameters.AddWithValue("Id", Id);

          using (SqlDataReader reader = command.ExecuteReader())
          {
            Employer employer = new Employer();

            if (reader.Read())
            {
              employer = (new Employer()
              {
                Id          = (int)reader["id"],
                Name        = (string)reader["Name"],
                City        = (string)reader["City"],
                EMail       = (string)reader["EMail"],
                Description = (string)reader["Description"]
              });
              return employer;
            }
            else
            {
              return null;
            }
          }
        }
      }
    }

    public void Add(Employer employer)
    {
      using (SqlConnection connection = new SqlConnection(connectionString))
      {
        connection.Open();

        using (SqlCommand command = new SqlCommand(queryInsert, connection))
        {
          command.Parameters.AddWithValue("Name",       employer.Name);
          command.Parameters.AddWithValue("City",       employer.City);
          command.Parameters.AddWithValue("EMail",      employer.EMail);
          command.Parameters.AddWithValue("City",       employer.City);
          command.Parameters.AddWithValue("Description", employer.Description);

          int connectId = (int)command.ExecuteScalar();

          //employer.Id = connectId;
          //return employer;
        }
      }
    }

    public void Remove(Employer employer)
    {
      using (SqlConnection connection = new SqlConnection(connectionString))
      {
        connection.Open();

        using (SqlCommand command = new SqlCommand(queryRemove, connection))
        {
          command.Parameters.AddWithValue("Id", employer.Id);

          command.ExecuteNonQuery();
        }
      }
    }

    public void Update(Employer employer)
    {
      using (SqlConnection connection = new SqlConnection(connectionString))
      {
        connection.Open();
        using (SqlCommand command = new SqlCommand(queryUpdate, connection))
        {
          command.Parameters.AddWithValue("Name", employer.Name);
          command.Parameters.AddWithValue("City", employer.City);
          command.Parameters.AddWithValue("EMail", employer.EMail);
          command.Parameters.AddWithValue("City", employer.City);
          command.Parameters.AddWithValue("Description", employer.Description);

          command.Parameters.AddWithValue("Id", employer.Id);

          command.ExecuteNonQuery();

        }
      }
    }

  }
}
