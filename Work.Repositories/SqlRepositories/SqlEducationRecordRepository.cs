﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Configuration;
using System.Data.SqlClient;
using Work.Entities;
using Work.DataLayer;
//using System.Data.Entity;

//namespace Work.Repositories.SqlRepositories
namespace Work.Repositories
{
  public class SqlEducationRecordRepository : IEducationRecordRepository
  {
    static string connectionString = ConfigurationManager.ConnectionStrings["ConnectToSQL"].ConnectionString;

    static string queryGetAll = "SELECT [Id],[ResumeId],[TypeOfEducationId],[College],[City],[FromDate],[ToDate],[Description] FROM [dbo].[EducationRecords]";
    static string queryGetAllId = "SELECT [Id],[ResumeId],[TypeOfEducationId],[College],[City],[FromDate],[ToDate],[Description] FROM [dbo].[EducationRecords] WHERE [Id] = @Id";
    //static string maxIdSelect = "SELECT MAX(Id) FROM [dbo].[EducationRecords] GROUP BY ID";
    static string queryInsert = "INSERT INTO [dbo].[EducationRecords] ([ResumeId],[TypeOfEducationId],[College],[City],[FromDate],[ToDate],[Description]) VALUES (@ResumeId,@TypeOfEducationId,@College,@City,@FromDate,@ToDate,@Description); SELECT CAST(SCOPE_IDENTITY() AS INT);";
    static string queryRemove = "DELETE FROM [dbo].[EducationRecords] WHERE [Id] = @Id";

    static string queryUpdate =
      "UPDATE [dbo].[EducationRecords] SET [ResumeId]=@ResumeId,[TypeOfEducationId]=@TypeOfEducationId,[College]=@College,[City]=@City,[FromDate]=@FromDate,[ToDate]=@ToDate,[Description]=@Description WHERE [Id] = @Id";

    public List<EducationRecord> GetAll()
    {
      using (SqlConnection connection = new SqlConnection(connectionString))
      {
        connection.Open();

        using (SqlCommand command = new SqlCommand(queryGetAll, connection))
        {
          using (SqlDataReader reader = command.ExecuteReader())
          {
            List<EducationRecord> educationrecords = new List<EducationRecord>();

            while (reader.Read())
            {
              educationrecords.Add(new EducationRecord()
              {
                Id                = (int)reader["Id"],
                ResumeId          = (int)reader["ResumeId"],
                TypeOfEducationId = (TypeOfEducation)reader["TypeOfEducationId"],
                College           = (string)reader["College"],
                City              = (string)reader["City"],
                FromDate          = (System.DateTime)reader["FromDate"],
                ToDate            = (System.DateTime)reader["ToDate"],
                Description       = (string)reader["Description"]
              });
            }
            return educationrecords;
          }
        }
      }
    }
    public EducationRecord Get(int Id)
    {
      using (SqlConnection connection = new SqlConnection(connectionString))
      {
        connection.Open();

        using (SqlCommand command = new SqlCommand(queryGetAllId, connection))
        {
          command.Parameters.AddWithValue("Id", Id);

          using (SqlDataReader reader = command.ExecuteReader())
          {
            EducationRecord educationrecord = new EducationRecord();

            if (reader.Read())
            {
              educationrecord = (new EducationRecord()
              {
                Id = (int)reader["Id"],
                ResumeId = (int)reader["ResumeId"],
                TypeOfEducationId = (TypeOfEducation)reader["TypeOfEducationId"],
                College = (string)reader["College"],
                City = (string)reader["City"],
                FromDate = (System.DateTime)reader["FromDate"],
                ToDate = (System.DateTime)reader["ToDate"],
                Description = (string)reader["Description"]
              });
              return educationrecord;
            }
            else
            {
              return null;
            }
          }
        }
      }
    }

    public void Add(EducationRecord educationrecord)
    {
      using (SqlConnection connection = new SqlConnection(connectionString))
      {
        connection.Open();

        using (SqlCommand command = new SqlCommand(queryInsert, connection))
        {
          command.Parameters.AddWithValue("ResumeId",           educationrecord.ResumeId);
          command.Parameters.AddWithValue("TypeOfEducationId",  educationrecord.TypeOfEducationId);
          command.Parameters.AddWithValue("College",            educationrecord.College);
          command.Parameters.AddWithValue("City",               educationrecord.City);
          command.Parameters.AddWithValue("FromDate",           educationrecord.FromDate);
          command.Parameters.AddWithValue("ToDate",             educationrecord.ToDate);
          command.Parameters.AddWithValue("Description",        educationrecord.Description);
          
          int connectId = (int)command.ExecuteScalar();

          //educationrecord.Id = connectId;
          //return educationrecord;
        }
      }
    }

    public void Remove(EducationRecord educationrecord)
    {
      using (SqlConnection connection = new SqlConnection(connectionString))
      {
        connection.Open();

        using (SqlCommand command = new SqlCommand(queryRemove, connection))
        {
          command.Parameters.AddWithValue("Id", educationrecord.Id);

          command.ExecuteNonQuery();
        }
      }
    }

    public void Update(EducationRecord educationrecord)
    {
      using (SqlConnection connection = new SqlConnection(connectionString))
      {
        connection.Open();
        using (SqlCommand command = new SqlCommand(queryUpdate, connection))
        {
          command.Parameters.AddWithValue("ResumeId",           educationrecord.ResumeId);
          command.Parameters.AddWithValue("TypeOfEducationId",  educationrecord.TypeOfEducationId);
          command.Parameters.AddWithValue("College",            educationrecord.College);
          command.Parameters.AddWithValue("City",               educationrecord.City);
          command.Parameters.AddWithValue("FromDate",           educationrecord.FromDate);
          command.Parameters.AddWithValue("ToDate",             educationrecord.ToDate);
          command.Parameters.AddWithValue("Description",        educationrecord.Description);

          command.Parameters.AddWithValue("Id",                 educationrecord.Id);

          command.ExecuteNonQuery();

        }
      }
    }

  }
}
