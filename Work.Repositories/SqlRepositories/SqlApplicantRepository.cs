﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Configuration;
using System.Data.SqlClient;
using Work.Entities;
using Work.DataLayer;
//using System.Data.Entity;

//namespace Work.Repositories.SqlRepositories
namespace Work.Repositories
{
  public class SqlApplicantRepository : IApplicantRepository
  {
    static string connectionString = ConfigurationManager.ConnectionStrings["ConnectToSQL"].ConnectionString;

    static string queryGetAll = "SELECT [Id],[FirstName],[LastName],[DateOfBirth],[City],[EMail] FROM [dbo].[Applicants]";
    static string queryGetAllId = "SELECT [Id],[FirstName],[LastName],[DateOfBirth],[City],[EMail] FROM [dbo].[Applicants] WHERE [Id] = @Id";
    //static string maxIdSelect = "SELECT MAX(Id) FROM [dbo].[Applicants] GROUP BY ID";
    static string queryInsert = "INSERT INTO [dbo].[Applicants] ([FirstName],[LastName],[DateOfBirth],[City],[EMail]) VALUES (@FirstName,@LastName,@DateOfBirth,@City,@EMail); SELECT CAST(SCOPE_IDENTITY() AS INT);";
    static string queryRemove = "DELETE FROM [dbo].[Applicants] WHERE [Id] = @Id";

    static string queryUpdate =
      "UPDATE [dbo].[Applicants] SET [FirstName]=@FirstName,[LastName]=@LastName,[DateOfBirth]=@DateOfBirth,[City]=@City,[EMail]=@EMail WHERE [Id] = @Id";

    public List<Applicant> GetAll()
    {
      using (SqlConnection connection = new SqlConnection(connectionString))
      {
        connection.Open();

        using (SqlCommand command = new SqlCommand(queryGetAll, connection))
        {
          using (SqlDataReader reader = command.ExecuteReader())
          {
            List<Applicant> applicants = new List<Applicant>();

            while (reader.Read())
            {
              applicants.Add(new Applicant()
              {
                Id = (int)reader["id"],
                FirstName = (string)reader["FirstName"],
                LastName = (string)reader["LastName"],
                DateOfBirth = (System.DateTime)reader["DateOfBirth"],
                City = (string)reader["City"],
                EMail = (string)reader["EMail"]
              });
            }
            return applicants;
          }
        }
      }
    }
    public Applicant Get(int Id)
    {
      using (SqlConnection connection = new SqlConnection(connectionString))
      {
        connection.Open();

        using (SqlCommand command = new SqlCommand(queryGetAllId, connection))
        {
          command.Parameters.AddWithValue("Id", Id);

          using (SqlDataReader reader = command.ExecuteReader())
          {
            Applicant applicant = new Applicant();

            if (reader.Read())
            {
              applicant = (new Applicant()
              {
                Id = (int)reader["Id"],
                FirstName = (string)reader["FirstName"],
                LastName = (string)reader["LastName"],
                DateOfBirth = (System.DateTime)reader["DateOfBirth"],
                City = (string)reader["City"],
                EMail = (string)reader["EMail"]
              });
              return applicant;
            }
            else
            {
              return null;
            }
          }
        }
      }
    }

    public void Add(Applicant applicant)
    {
      using (SqlConnection connection = new SqlConnection(connectionString))
      {
        connection.Open();

        using (SqlCommand command = new SqlCommand(queryInsert, connection))
        {
          command.Parameters.AddWithValue("FirstName", applicant.FirstName);
          command.Parameters.AddWithValue("LastName", applicant.LastName);
          command.Parameters.AddWithValue("DateOfBirth", applicant.DateOfBirth);
          command.Parameters.AddWithValue("City", applicant.City);
          command.Parameters.AddWithValue("EMail", applicant.EMail);

          int connectId = (int)command.ExecuteScalar();

          //applicant.Id = connectId;
          //return applicant;
        }
      }
    }

    public void Remove(Applicant applicant)
    {
      using (SqlConnection connection = new SqlConnection(connectionString))
      {
        connection.Open();

        using (SqlCommand command = new SqlCommand(queryRemove, connection))
        {
          command.Parameters.AddWithValue("Id", applicant.Id);

          command.ExecuteNonQuery();
        }
      }
    }

    public void Update(Applicant applicant)
    {
      using (SqlConnection connection = new SqlConnection(connectionString))
      {
        connection.Open();
        using (SqlCommand command = new SqlCommand(queryUpdate, connection))
        {
          command.Parameters.AddWithValue("FirstName", applicant.FirstName);
          command.Parameters.AddWithValue("LastName", applicant.LastName);
          command.Parameters.AddWithValue("DateOfBirth", applicant.DateOfBirth);
          command.Parameters.AddWithValue("City", applicant.City);
          command.Parameters.AddWithValue("EMail", applicant.EMail);

          command.Parameters.AddWithValue("Id", applicant.Id);

          command.ExecuteNonQuery();

        }
      }
    }

  }
}
