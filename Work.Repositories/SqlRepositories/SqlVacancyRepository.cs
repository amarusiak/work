﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Configuration;
using System.Data.SqlClient;
using Work.Entities;
using Work.DataLayer;
//using System.Data.Entity;

//namespace Work.Repositories.SqlRepositories
namespace Work.Repositories
{
  public class SqlVacancyRepository : IVacancyRepository
  {
    static string connectionString = ConfigurationManager.ConnectionStrings["ConnectToSQL"].ConnectionString;

    static string queryGetAll = "SELECT [Id],[EmployerId],[Position],[City],[EconomySectorId],[TypeOfJobId],[YearsOfExperience],[TypeOfEducationId],[Salary],[Description] FROM [dbo].[Vacancies]";
    //static string queryGetAll =
    //  "SELECT vac.Id, emp.Name,vac.Position,vac.City,vac.EconomySectorId,vac.TypeOfJobId,vac.YearsOfExperience,vac.TypeOfEducationId,vac.Salary,vac.Description " +
    //  "FROM [dbo].[Vacancies] vac " +
    //  "LEFT JOIN [dbo].[Employers] emp ON vac.EmployerId=emp.Id";

    static string queryGetAllId = "SELECT [Id],[EmployerId],[Position],[City],[EconomySectorId],[TypeOfJobId],[YearsOfExperience],[TypeOfEducationId],[Salary],[Description] FROM [dbo].[Vacancies] WHERE [Id] = @Id";
    //static string maxIdSelect = "SELECT MAX(Id) FROM [dbo].[Vacancys] GROUP BY ID";

    //static string queryInsert = "INSERT INTO [dbo].[Vacancies] ([EmployerId], [Position], [City], [EconomySectorId], [TypeOfJobId], [YearsOfExperience], [TypeOfEducationId], [Salary], [Description]) VALUES (@EmployerId, @Position, @City, @EconomySectorId, @TypeOfJobId, @YearsOfExperience,@TypeOfEducationId,@Salary,@Description); SELECT CAST(SCOPE_IDENTITY() AS INT);";
    static string queryInsert = "INSERT INTO [dbo].[Vacancies] ([EmployerId], [Position], [City], [EconomySectorId], [TypeOfJobId], [YearsOfExperience], [TypeOfEducationId], [Salary], [Description]) VALUES (@EmployerId, @Position, @City, @EconomySectorId, @TypeOfJobId, @YearsOfExperience, @TypeOfEducationId, @Salary, @Description); SELECT CAST(SCOPE_IDENTITY() AS INT)";
    
    static string queryRemove = "DELETE FROM [dbo].[Vacancies] WHERE [Id] = @Id";

    static string queryUpdate =
      "UPDATE [dbo].[Vacancies] SET [EmployerId]=@EmployerId,[Position]=@Position,[City]=@City,[EconomySectorId]=@EconomySectorId,[TypeOfJobId]=@TypeOfJobId," + 
      "[YearsOfExperience]=@YearsOfExperience,[TypeOfEducationId]=@TypeOfEducationId,[Salary]=@Salary,[Description]=@Description WHERE [Id] = @Id";

    public List<Vacancy> GetAll()
    {
      using (SqlConnection connection = new SqlConnection(connectionString))
      {
        connection.Open();

        using (SqlCommand command = new SqlCommand(queryGetAll, connection))
        {
          using (SqlDataReader reader = command.ExecuteReader())
          {
            List<Vacancy> vacancys = new List<Vacancy>();

            while (reader.Read())
            {
              vacancys.Add(new Vacancy()
              {
                Id                    = (int)reader["Id"],
                EmployerId            = (int)reader["EmployerId"],
                Position              = (string)reader["Position"],
                City                  = (string)reader["City"],
                EconomySectorId       = (int)reader["EconomySectorId"],
                TypeOfJobId           = (TypeOfJob)reader["TypeOfJobId"],
                YearsOfExperience     = (int)reader["YearsOfExperience"],
                TypeOfEducationId     = (TypeOfEducation)reader["TypeOfEducationId"],
                Salary                = (int)reader["Salary"],
                Description           = (string)reader["Description"]
              });
            }
            return vacancys;
          }
        }
      }
    }
    public Vacancy Get(int Id)
    {
      using (SqlConnection connection = new SqlConnection(connectionString))
      {
        connection.Open();

        using (SqlCommand command = new SqlCommand(queryGetAllId, connection))
        {
          command.Parameters.AddWithValue("Id", Id);

          using (SqlDataReader reader = command.ExecuteReader())
          {
            Vacancy vacancy = new Vacancy();

            if (reader.Read())
            {
              vacancy = (new Vacancy()
              {
                Id = (int)reader["Id"],
                EmployerId = (int)reader["EmployerId"],
                Position = (string)reader["Position"],
                City = (string)reader["City"],
                EconomySectorId = (int)reader["EconomySectorId"],
                TypeOfJobId = (TypeOfJob)reader["TypeOfJobId"],
                YearsOfExperience = (int)reader["YearsOfExperience"],
                TypeOfEducationId = (TypeOfEducation)reader["TypeOfEducationId"],
                Salary = (int)reader["Salary"],
                Description = (string)reader["Description"]
              });
              return vacancy;
            }
            else
            {
              return null;
            }
          }
        }
      }
    }

    public void Add(Vacancy vacancy)
    {
      using (SqlConnection connection = new SqlConnection(connectionString))
      {
        connection.Open();

        using (SqlCommand command = new SqlCommand(queryInsert, connection))
        {
          command.Parameters.AddWithValue("EmployerId", vacancy.EmployerId);
          command.Parameters.AddWithValue("Position", vacancy.Position);
          command.Parameters.AddWithValue("City", vacancy.City);
          command.Parameters.AddWithValue("EconomySectorId", vacancy.EconomySectorId);
          command.Parameters.AddWithValue("TypeOfJobId", vacancy.TypeOfJobId);
          command.Parameters.AddWithValue("YearsOfExperience", vacancy.YearsOfExperience);
          command.Parameters.AddWithValue("TypeOfEducationId", vacancy.TypeOfEducationId);
          command.Parameters.AddWithValue("Salary", vacancy.Salary);
          command.Parameters.AddWithValue("Description", vacancy.Description);

          int connectId = (int)command.ExecuteScalar();

          //vacancy.Id = connectId;
          //return vacancy;
        }
      }
    }

    public void Remove(Vacancy vacancy)
    {
      using (SqlConnection connection = new SqlConnection(connectionString))
      {
        connection.Open();

        using (SqlCommand command = new SqlCommand(queryRemove, connection))
        {
          command.Parameters.AddWithValue("Id", vacancy.Id);

          command.ExecuteNonQuery();
        }
      }
    }

    public void Update(Vacancy vacancy)
    {
      using (SqlConnection connection = new SqlConnection(connectionString))
      {
        connection.Open();
        using (SqlCommand command = new SqlCommand(queryUpdate, connection))
        {
          command.Parameters.AddWithValue("EmployerId", vacancy.EmployerId);
          command.Parameters.AddWithValue("Position", vacancy.Position);
          command.Parameters.AddWithValue("City", vacancy.City);
          command.Parameters.AddWithValue("EconomySectorId", vacancy.EconomySectorId);
          command.Parameters.AddWithValue("TypeOfJobId", vacancy.TypeOfJobId);
          command.Parameters.AddWithValue("YearsOfExperience", vacancy.YearsOfExperience);
          command.Parameters.AddWithValue("TypeOfEducationId", vacancy.TypeOfEducationId);
          command.Parameters.AddWithValue("Salary", vacancy.Salary);
          command.Parameters.AddWithValue("Description", vacancy.Description);

          command.Parameters.AddWithValue("Id", vacancy.Id);

          command.ExecuteNonQuery();

        }
      }
    }


  }
}
