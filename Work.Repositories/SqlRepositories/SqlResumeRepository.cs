﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Configuration;
using System.Data.SqlClient;
using Work.Entities;
using Work.DataLayer;
//using System.Data.Entity;

//namespace Work.Repositories.SqlRepositories
namespace Work.Repositories
{
  public class SqlResumeRepository : IResumeRepository
  {
    static string connectionString = ConfigurationManager.ConnectionStrings["ConnectToSQL"].ConnectionString;

    static string queryGetAll = "SELECT [Id],[ApplicantId],[EconomySectorId],[City],[TypeOfJobId],[Salary],[Description] FROM [dbo].[Resumes]";
    static string queryGetAllId = "SELECT [Id],[ApplicantId],[EconomySectorId],[City],[TypeOfJobId],[Salary],[Description] FROM [dbo].[Resumes] WHERE [Id] = @Id";
    //static string maxIdSelect = "SELECT MAX(Id) FROM [dbo].[Resumes] GROUP BY ID";
    static string queryInsert = "INSERT INTO [dbo].[Resumes] ([ApplicantId],[EconomySectorId],[City],[TypeOfJobId],[Salary],[Description]) VALUES (@ApplicantId,@EconomySectorId,@City,@TypeOfJobId,@Salary,@Description); SELECT CAST(SCOPE_IDENTITY() AS INT);";
    static string queryRemove = "DELETE FROM [dbo].[Resumes] WHERE [Id] = @Id";

    static string queryUpdate =
      "UPDATE [dbo].[Resumes] SET [ApplicantId]=@ApplicantId,[EconomySectorId]=@EconomySectorId,[City]=@City,[TypeOfJobId]=@TypeOfJobId,[Salary]=@Salary,[Description]=@Description WHERE [Id] = @Id";

    public List<Resume> GetAll()
    {
      using (SqlConnection connection = new SqlConnection(connectionString))
      {
        connection.Open();

        using (SqlCommand command = new SqlCommand(queryGetAll, connection))
        {
          using (SqlDataReader reader = command.ExecuteReader())
          {
            List<Resume> resumes = new List<Resume>();

            while (reader.Read())
            {
              resumes.Add(new Resume()
              {
                Id                = (int)reader["Id"],
                ApplicantId       = (int)reader["ApplicantId"],
                EconomySectorId   = (int)reader["EconomySectorId"],
                City              = (string)reader["City"],
                TypeOfJobId       = (TypeOfJob)reader["TypeOfJobId"],
                Salary            = (int)reader["Salary"],
                Description       = (string)reader["Description"]
              });
            }
            return resumes;
          }
        }
      }
    }
    public Resume Get(int Id)
    {
      using (SqlConnection connection = new SqlConnection(connectionString))
      {
        connection.Open();

        using (SqlCommand command = new SqlCommand(queryGetAllId, connection))
        {
          command.Parameters.AddWithValue("Id", Id);

          using (SqlDataReader reader = command.ExecuteReader())
          {
            Resume resume = new Resume();

            if (reader.Read())
            {
              resume = (new Resume()
              {
                Id = (int)reader["Id"],
                ApplicantId = (int)reader["ApplicantId"],
                EconomySectorId = (int)reader["EconomySectorId"],
                City = (string)reader["City"],
                TypeOfJobId = (TypeOfJob)reader["TypeOfJobId"],
                Salary = (int)reader["Salary"],
                Description = (string)reader["Description"]
              });
              return resume;
            }
            else
            {
              return null;
            }
          }
        }
      }
    }

    public void Add(Resume resume)
    {
      using (SqlConnection connection = new SqlConnection(connectionString))
      {
        connection.Open();

        using (SqlCommand command = new SqlCommand(queryInsert, connection))
        {
          command.Parameters.AddWithValue("ApplicantId", resume.ApplicantId);
          command.Parameters.AddWithValue("EconomySectorId", resume.EconomySectorId);
          command.Parameters.AddWithValue("City", resume.City);
          command.Parameters.AddWithValue("TypeOfJobId", resume.TypeOfJobId);
          command.Parameters.AddWithValue("Salary", resume.Salary);
          command.Parameters.AddWithValue("Description", resume.Description);

          int connectId = (int)command.ExecuteScalar();

          //resume.Id = connectId;
          //return resume;
        }
      }
    }

    public void Remove(Resume resume)
    {
      using (SqlConnection connection = new SqlConnection(connectionString))
      {
        connection.Open();

        using (SqlCommand command = new SqlCommand(queryRemove, connection))
        {
          command.Parameters.AddWithValue("Id", resume.Id);

          command.ExecuteNonQuery();
        }
      }
    }

    public void Update(Resume resume)
    {
      using (SqlConnection connection = new SqlConnection(connectionString))
      {
        connection.Open();
        using (SqlCommand command = new SqlCommand(queryUpdate, connection))
        {
          command.Parameters.AddWithValue("ApplicantId", resume.ApplicantId);
          command.Parameters.AddWithValue("EconomySectorId", resume.EconomySectorId);
          command.Parameters.AddWithValue("City", resume.City);
          command.Parameters.AddWithValue("TypeOfJobId", resume.TypeOfJobId);
          command.Parameters.AddWithValue("Salary", resume.Salary);
          command.Parameters.AddWithValue("Description", resume.Description);

          command.Parameters.AddWithValue("Id", resume.Id);

          command.ExecuteNonQuery();

        }
      }
    }


  }
}
