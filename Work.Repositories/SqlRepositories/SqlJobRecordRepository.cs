﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Configuration;
using System.Data.SqlClient;
using Work.Entities;
using Work.DataLayer;
//using System.Data.Entity;

//namespace Work.Repositories.SqlRepositories
namespace Work.Repositories
{
  public class SqlJobRecordRepository : IJobRecordRepository
  {
    static string connectionString = ConfigurationManager.ConnectionStrings["ConnectToSQL"].ConnectionString;

    static string queryGetAll = "SELECT [Id],[ResumeId],[Company],[City],[Position],[FromDate],[ToDate],[Description] FROM [dbo].[JobRecords]";
    static string queryGetAllId = "SELECT [Id],[ResumeId],[Company],[City],[Position],[FromDate],[ToDate],[Description] FROM [dbo].[JobRecords] WHERE [Id] = @Id";
    //static string maxIdSelect = "SELECT MAX(Id) FROM [dbo].[JobRecords] GROUP BY ID";
    static string queryInsert = "INSERT INTO [dbo].[JobRecords] ([ResumeId],[Company],[City],[Position],[FromDate],[ToDate],[Description]) VALUES (@ResumeId,@Company,@City,@Position,@FromDate,@ToDate,@Description); SELECT CAST(SCOPE_IDENTITY() AS INT);";
    static string queryRemove = "DELETE FROM [dbo].[JobRecords] WHERE [Id] = @Id";

    static string queryUpdate =
      "UPDATE [dbo].[JobRecords] SET [ResumeId]=@ResumeId,[Company]=@Company,[City]=@City,[Position]=@Position,[FromDate]=@FromDate,[ToDate]=@ToDate,[Description]=@Description WHERE [Id] = @Id";

    public List<JobRecord> GetAll()
    {
      using (SqlConnection connection = new SqlConnection(connectionString))
      {
        connection.Open();

        using (SqlCommand command = new SqlCommand(queryGetAll, connection))
        {
          using (SqlDataReader reader = command.ExecuteReader())
          {
            List<JobRecord> jobrecords = new List<JobRecord>();

            while (reader.Read())
            {
              jobrecords.Add(new JobRecord()
              {
                Id      = (int)reader["Id"],
                ResumeId = (int)reader["ResumeId"],
                Company = (string)reader["Company"],
                City = (string)reader["City"],
                Position = (string)reader["Position"],
                FromDate = (System.DateTime)reader["FromDate"],
                ToDate = (System.DateTime)reader["ToDate"],
                Description = (string)reader["Description"]
              });
            }
            return jobrecords;
          }
        }
      }
    }
    public JobRecord Get(int Id)
    {
      using (SqlConnection connection = new SqlConnection(connectionString))
      {
        connection.Open();

        using (SqlCommand command = new SqlCommand(queryGetAllId, connection))
        {
          command.Parameters.AddWithValue("Id", Id);

          using (SqlDataReader reader = command.ExecuteReader())
          {
            JobRecord jobrecord = new JobRecord();

            if (reader.Read())
            {
              jobrecord = (new JobRecord()
              {
                Id            = (int)reader["Id"],
                ResumeId      = (int)reader["ResumeId"],
                Company       = (string)reader["Company"],
                City          = (string)reader["City"],
                Position      = (string)reader["Position"],
                FromDate      = (System.DateTime)reader["FromDate"],
                ToDate        = (System.DateTime)reader["ToDate"],
                Description   = (string)reader["Description"]
              });
              return jobrecord;
            }
            else
            {
              return null;
            }
          }
        }
      }
    }

    public void Add(JobRecord jobrecord)
    {
      using (SqlConnection connection = new SqlConnection(connectionString))
      {
        connection.Open();

        using (SqlCommand command = new SqlCommand(queryInsert, connection))
        {
          command.Parameters.AddWithValue("ResumeId", jobrecord.ResumeId);
          command.Parameters.AddWithValue("Company",  jobrecord.Company);
          command.Parameters.AddWithValue("City",     jobrecord.City);
          command.Parameters.AddWithValue("Position", jobrecord.Position);
          command.Parameters.AddWithValue("FromDate", jobrecord.FromDate);
          command.Parameters.AddWithValue("ToDate",   jobrecord.ToDate);
          command.Parameters.AddWithValue("Description", jobrecord.Description);

          int connectId = (int)command.ExecuteScalar();

          //jobrecord.Id = connectId;
          //return jobrecord;
        }
      }
    }

    public void Remove(JobRecord jobrecord)
    {
      using (SqlConnection connection = new SqlConnection(connectionString))
      {
        connection.Open();

        using (SqlCommand command = new SqlCommand(queryRemove, connection))
        {
          command.Parameters.AddWithValue("Id", jobrecord.Id);

          command.ExecuteNonQuery();
        }
      }
    }

    public void Update(JobRecord jobrecord)
    {
      using (SqlConnection connection = new SqlConnection(connectionString))
      {
        connection.Open();
        using (SqlCommand command = new SqlCommand(queryUpdate, connection))
        {
          command.Parameters.AddWithValue("ResumeId", jobrecord.ResumeId);
          command.Parameters.AddWithValue("Company", jobrecord.Company);
          command.Parameters.AddWithValue("City", jobrecord.City);
          command.Parameters.AddWithValue("Position", jobrecord.Position);
          command.Parameters.AddWithValue("FromDate", jobrecord.FromDate);
          command.Parameters.AddWithValue("ToDate", jobrecord.ToDate);
          command.Parameters.AddWithValue("Description", jobrecord.Description);

          command.Parameters.AddWithValue("Id", jobrecord.Id);

          command.ExecuteNonQuery();

        }
      }
    }
  }
}
