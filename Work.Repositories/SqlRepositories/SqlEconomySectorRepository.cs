﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Configuration;
using System.Data.SqlClient;
using Work.Entities;
using Work.DataLayer;
//using System.Data.Entity;

//namespace Work.Repositories.SqlRepositories
namespace Work.Repositories
{
  public class SqlEconomySectorRepository : IEconomySectorRepository
  {
    static string connectionString = ConfigurationManager.ConnectionStrings["ConnectToSQL"].ConnectionString;

    static string queryGetAll = "SELECT [Id],[Name] FROM [dbo].[EconomySectors]";
    static string queryGetAllId = "SELECT [Id],[Name] FROM [dbo].[EconomySectors] WHERE [Id] = @Id";
    //static string maxIdSelect = "SELECT MAX(Id) FROM [dbo].[EconomySectors] GROUP BY ID";
    static string queryInsert = "INSERT INTO [dbo].[EconomySectors] ([Name]) VALUES (@Name); SELECT CAST(SCOPE_IDENTITY() AS INT);";
    static string queryRemove = "DELETE FROM [dbo].[EconomySectors] WHERE [Id] = @Id";

    static string queryUpdate =
      "UPDATE [dbo].[EconomySectors] SET [Name]=@Name WHERE [Id] = @Id";


    public List<EconomySector> GetAll()
    {
      using (SqlConnection connection = new SqlConnection(connectionString))
      {
        connection.Open();

        using (SqlCommand command = new SqlCommand(queryGetAll, connection))
        {
          using (SqlDataReader reader = command.ExecuteReader())
          {
            List<EconomySector> economysectors = new List<EconomySector>();

            while (reader.Read())
            {
              economysectors.Add(new EconomySector()
              {
                Id = (int)reader["Id"],
                Name = (string)reader["Name"]
              });
            }
            return economysectors;
          }
        }
      }
    }
    public EconomySector Get(int Id)
    {
      using (SqlConnection connection = new SqlConnection(connectionString))
      {
        connection.Open();

        using (SqlCommand command = new SqlCommand(queryGetAllId, connection))
        {
          command.Parameters.AddWithValue("Id", Id);

          using (SqlDataReader reader = command.ExecuteReader())
          {
            EconomySector economysector = new EconomySector();

            if (reader.Read())
            {
              economysector = (new EconomySector()
              {
                Id = (int)reader["Id"],
                Name = (string)reader["Name"]
              });
              return economysector;
            }
            else
            {
              return null;
            }
          }
        }
      }
    }

    public void Add(EconomySector economysector)
    {
      using (SqlConnection connection = new SqlConnection(connectionString))
      {
        connection.Open();

        using (SqlCommand command = new SqlCommand(queryInsert, connection))
        {
          command.Parameters.AddWithValue("Name", economysector.Name);

          int connectId = (int)command.ExecuteScalar();

          //applicant.Id = connectId;
          //return applicant;
        }
      }
    }

    public void Remove(EconomySector economysector)
    {
      using (SqlConnection connection = new SqlConnection(connectionString))
      {
        connection.Open();

        using (SqlCommand command = new SqlCommand(queryRemove, connection))
        {
          command.Parameters.AddWithValue("Id", economysector.Id);

          command.ExecuteNonQuery();
        }
      }
    }

    public void Update(EconomySector economysector)
    {
      using (SqlConnection connection = new SqlConnection(connectionString))
      {
        connection.Open();
        using (SqlCommand command = new SqlCommand(queryUpdate, connection))
        {
          command.Parameters.AddWithValue("Name", economysector.Name);

          command.Parameters.AddWithValue("Id", economysector.Id);

          command.ExecuteNonQuery();

        }
      }
    }

  }
}
