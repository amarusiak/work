﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Work.Entities;

namespace Work.Repositories
{
  public interface IEmployerRepository
  {
    List<Employer> GetAll();
    Employer Get(int id);

    //void Add(string title);
    void Add(Employer employer);

    //void Remove(int id);
    void Remove(Employer employer);

    void Update(Employer employer);
  }
}
