﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Work.Entities;

namespace Work.Repositories
{
  public interface IJobRecordRepository
  {
    List<JobRecord> GetAll();
    JobRecord Get(int id);

    //void Add(string title);
    void Add(JobRecord jobrecord);

    //void Remove(int id);
    void Remove(JobRecord jobrecord);

    void Update(JobRecord jobrecord);
  }
}
