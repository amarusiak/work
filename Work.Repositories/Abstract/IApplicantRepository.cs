﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Work.Entities;

namespace Work.Repositories
{
  public interface IApplicantRepository
  {
      List<Applicant> GetAll();
      Applicant Get(int id);

      //void Add(string title);
      void Add(Applicant applicant);

      //void Remove(int id);
      void Remove(Applicant applicant);

      void Update(Applicant applicant);
  }
}
