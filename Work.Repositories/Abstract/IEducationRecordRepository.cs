﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Work.Entities;

namespace Work.Repositories
{
  public interface IEducationRecordRepository
  {
    List<EducationRecord> GetAll();
    EducationRecord Get(int id);

    //void Add(string title);
    void Add(EducationRecord educationrecord);

    //void Remove(int id);
    void Remove(EducationRecord educationrecord);

    void Update(EducationRecord educationrecord);
  }
}
