﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Work.Entities;

namespace Work.Repositories
{
  public interface IUserRepository
  {
    List<User> GetAll();
    User Get(int id);

    //void Add(string title);
    void Add(User user);

    //void Remove(int id);
    void Remove(User user);

    void Update(User user);
  }
}
