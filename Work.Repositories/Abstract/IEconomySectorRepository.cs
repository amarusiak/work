﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Work.Entities;

namespace Work.Repositories
{
  public interface IEconomySectorRepository
  {
    List<EconomySector> GetAll();
    EconomySector Get(int id);

    void Add(EconomySector economysector);

    void Remove(EconomySector economysector);

    void Update(EconomySector economysector);
  }
}
