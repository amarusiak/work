﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Work.Entities;

namespace Work.Repositories
{
  public interface IVacancyRepository
  {
    List<Vacancy> GetAll();
    Vacancy Get(int id);

    //void Add(string title);
    void Add(Vacancy vacancy);

    //void Remove(int id);
    void Remove(Vacancy vacancy);

    void Update(Vacancy vacancy);
  }
}
