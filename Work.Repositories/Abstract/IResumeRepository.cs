﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Work.Entities;

namespace Work.Repositories
{
  public interface IResumeRepository
  {
    List<Resume> GetAll();
    Resume Get(int id);

    //void Add(string title);
    void Add(Resume resume);

    //void Remove(int id);
    void Remove(Resume resume);

    void Update(Resume resume);
  }
}
