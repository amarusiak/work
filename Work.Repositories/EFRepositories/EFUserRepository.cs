﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Work.Entities;
using Work.DataLayer;
using System.Data.Entity;

namespace Work.Repositories
{
  public class EFUserRepository : IUserRepository
  {

    public List<User> GetAll()
    {
      using (var ctx = new WorkModelContext())
      {
        List<User> users = ctx.Users.ToList();
        return users;
      }
    }

    public User Get(int id)
    {
      using (var ctx = new WorkModelContext())
      {
        User user = ctx.Users.FirstOrDefault(x => x.Id == id);
        return user;
      }
    }

    public void Add(User user)
    {
      using (var ctx = new WorkModelContext())
      {
        ctx.Users.Add(user);
        ctx.SaveChanges();
      }
    }

    public void Remove(User user)
    {
      using (var ctx = new WorkModelContext())
      {
        ctx.Users.Remove(user);
        ctx.SaveChanges();
      }
    }

    public void Update(User user)
    {
      using (var ctx = new WorkModelContext())
      {
        ctx.Users.Attach(user);
        ctx.Entry(user).State = EntityState.Modified;
        ctx.SaveChanges();
      }
    }
  }
}
