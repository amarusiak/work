﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Work.Entities;
using Work.DataLayer;
using System.Data.Entity;

namespace Work.Repositories
{
  public class EFEducationRecordRepository : IEducationRecordRepository
  {
    public List<EducationRecord> GetAll()
    {
      using (var ctx = new WorkModelContext())
      {
        List<EducationRecord> educationrecords = ctx.EducationRecords.ToList();
        return educationrecords;
      }
    }

    public EducationRecord Get(int id)
    {
      using (var ctx = new WorkModelContext())
      {
        EducationRecord educationrecord = ctx.EducationRecords.FirstOrDefault(x => x.Id == id);
        return educationrecord;
      }
    }

    public void Add(EducationRecord educationrecord)
    {
      using (var ctx = new WorkModelContext())
      {
        ctx.EducationRecords.Add(educationrecord);
        ctx.SaveChanges();
      }
    }

    public void Remove(EducationRecord educationrecord)
    {
      using (var ctx = new WorkModelContext())
      {
        ctx.EducationRecords.Remove(educationrecord);
        ctx.SaveChanges();
      }
    }

    public void Update(EducationRecord educationrecord)
    {
      using (var ctx = new WorkModelContext())
      {
        ctx.EducationRecords.Attach(educationrecord);
        ctx.Entry(educationrecord).State = EntityState.Modified;
        ctx.SaveChanges();
      }
    }
  }
}
