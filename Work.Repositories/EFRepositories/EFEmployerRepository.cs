﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Work.Entities;
using Work.DataLayer;
using System.Data.Entity;

namespace Work.Repositories
{
  public class EFEmployerRepository : IEmployerRepository
  {
    public List<Employer> GetAll()
    {
      using (var ctx = new WorkModelContext())
      {
        List<Employer> employers = ctx.Employers.ToList();
        return employers;
      }
    }

    public Employer Get(int id)
    {
      using (var ctx = new WorkModelContext())
      {
        Employer employer = ctx.Employers.FirstOrDefault(x => x.Id == id);
        return employer;
      }
    }

    public void Add(Employer employer)
    {
      using (var ctx = new WorkModelContext())
      {
        ctx.Employers.Add(employer);
        ctx.SaveChanges();
      }
    }

    public void Remove(Employer employer)
    {
      using (var ctx = new WorkModelContext())
      {
        ctx.Employers.Remove(employer);
        ctx.SaveChanges();
      }
    }

    public void Update(Employer employer)
    {
      using (var ctx = new WorkModelContext())
      {
        ctx.Employers.Attach(employer);
        ctx.Entry(employer).State = EntityState.Modified;
        ctx.SaveChanges();
      }
    }
  }
}
