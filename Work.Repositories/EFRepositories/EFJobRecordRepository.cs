﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Work.Entities;
using Work.DataLayer;
using System.Data.Entity;

namespace Work.Repositories
{
  public class EFJobRecordRepository : IJobRecordRepository
  {
    public List<JobRecord> GetAll()
    {
      using (var ctx = new WorkModelContext())
      {
        List<JobRecord> jobrecords = ctx.JobRecords.ToList();
        return jobrecords;
      }
    }

    public JobRecord Get(int id)
    {
      using (var ctx = new WorkModelContext())
      {
        JobRecord jobrecord = ctx.JobRecords.FirstOrDefault(x => x.Id == id);
        return jobrecord;
      }
    }

    public void Add(JobRecord jobrecord)
    {
      using (var ctx = new WorkModelContext())
      {
        ctx.JobRecords.Add(jobrecord);
        ctx.SaveChanges();
      }
    }

    public void Remove(JobRecord jobrecord)
    {
      using (var ctx = new WorkModelContext())
      {
        ctx.JobRecords.Remove(jobrecord);
        ctx.SaveChanges();
      }
    }

    public void Update(JobRecord jobrecord)
    {
      using (var ctx = new WorkModelContext())
      {
        ctx.JobRecords.Attach(jobrecord);
        ctx.Entry(jobrecord).State = EntityState.Modified;
        ctx.SaveChanges();
      }
    }
  }
}
