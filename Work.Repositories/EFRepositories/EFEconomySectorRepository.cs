﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Work.Entities;
using Work.DataLayer;
using System.Data.Entity;

namespace Work.Repositories
{
  public class EFEconomySectorRepository : IEconomySectorRepository
  {
    public List<EconomySector> GetAll()
    {
      using (var ctx = new WorkModelContext())
      {
        List<EconomySector> economysectors = ctx.EconomySectors.ToList();
        return economysectors;
      }
    }

    public EconomySector Get(int id)
    {
      using (var ctx = new WorkModelContext())
      {
        EconomySector economysector = ctx.EconomySectors.FirstOrDefault(x => x.Id == id);
        return economysector;
      }
    }

    public void Add(EconomySector economysector)
    {
      using (var ctx = new WorkModelContext())
      {
        ctx.EconomySectors.Add(economysector);
        ctx.SaveChanges();
      }
    }

    public void Remove(EconomySector economysector)
    {
      using (var ctx = new WorkModelContext())
      {
        ctx.EconomySectors.Remove(economysector);
        ctx.SaveChanges();
      }
    }

    public void Update(EconomySector economysector)
    {
      using (var ctx = new WorkModelContext())
      {
        ctx.EconomySectors.Attach(economysector);
        ctx.Entry(economysector).State = EntityState.Modified;
        ctx.SaveChanges();
      }
    }
  }
}
