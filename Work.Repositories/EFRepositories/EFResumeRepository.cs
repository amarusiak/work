﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Work.Entities;
using Work.DataLayer;
using System.Data.Entity;

namespace Work.Repositories
{
  public class EFResumeRepository : IResumeRepository
  {

    public List<Resume> GetAll()
    {
      using (var ctx = new WorkModelContext())
      {
        List<Resume> resumes = ctx.Resumes.ToList();
        return resumes;
      }
    }

    public Resume Get(int id)
    {
      using (var ctx = new WorkModelContext())
      {
        Resume resume = ctx.Resumes.FirstOrDefault(x => x.Id == id);
        return resume;
      }
    }

    public void Add(Resume resume)
    {
      using (var ctx = new WorkModelContext())
      {
        ctx.Resumes.Add(resume);
        ctx.SaveChanges();
      }
    }

    public void Remove(Resume resume)
    {
      using (var ctx = new WorkModelContext())
      {
        ctx.Resumes.Remove(resume);
        ctx.SaveChanges();
      }
    }

    public void Update(Resume resume)
    {
      using (var ctx = new WorkModelContext())
      {
        ctx.Resumes.Attach(resume);
        ctx.Entry(resume).State = EntityState.Modified;
        ctx.SaveChanges();
      }
    }
  }
}
