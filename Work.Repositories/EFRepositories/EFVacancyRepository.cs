﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Work.Entities;
using Work.DataLayer;
using System.Data.Entity;

namespace Work.Repositories
{
  public class EFVacancyRepository : IVacancyRepository
  {
    public List<Vacancy> GetAll()
    {
      using (var ctx = new WorkModelContext())
      {
        List<Vacancy> vacancys = ctx.Vacancys.ToList();
        return vacancys;
      }
    }

    public Vacancy Get(int id)
    {
      using (var ctx = new WorkModelContext())
      {
        Vacancy vacancy = ctx.Vacancys.FirstOrDefault(x => x.Id == id);
        return vacancy;
      }
    }

    public void Add(Vacancy vacancy)
    {
      using (var ctx = new WorkModelContext())
      {
        ctx.Vacancys.Add(vacancy);
        ctx.SaveChanges();
      }
    }

    public void Remove(Vacancy vacancy)
    {
      using (var ctx = new WorkModelContext())
      {
        ctx.Vacancys.Remove(vacancy);
        ctx.SaveChanges();
      }
    }

    public void Update(Vacancy vacancy)
    {
      using (var ctx = new WorkModelContext())
      {
        ctx.Vacancys.Attach(vacancy);
        ctx.Entry(vacancy).State = EntityState.Modified;
        ctx.SaveChanges();
      }
    }
  }
}
