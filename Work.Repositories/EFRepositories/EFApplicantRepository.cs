﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Work.Entities;
using Work.DataLayer;
using System.Data.Entity;

namespace Work.Repositories
{
  public class EFApplicantRepository : IApplicantRepository
  {

    //static string connectionString = ConfigurationManager.ConnectionStrings["ConnectToSQL"].ConnectionString;

    public List<Applicant> GetAll()
    {
      using (var ctx = new WorkModelContext())
      {
        List<Applicant> applicants = ctx.Applicants.ToList();
        return applicants;
      }
    }

    public Applicant Get(int id)
    {
      using (var ctx = new WorkModelContext())
      {
        Applicant applicant = ctx.Applicants.FirstOrDefault(x => x.Id == id);
        return applicant;
      }
    }

    public void Add(Applicant applicant)
    {
      using (var ctx = new WorkModelContext())
      {
        ctx.Applicants.Add(applicant);
        ctx.SaveChanges();
      }
    }

    public void Remove(Applicant applicant)
    {
      using (var ctx = new WorkModelContext())
      {
        ctx.Applicants.Remove(applicant);
        ctx.SaveChanges();
      }
    }

    public void Update(Applicant applicant)
    {
      using (var ctx = new WorkModelContext())
      {
        ctx.Applicants.Attach(applicant);
        ctx.Entry(applicant).State = EntityState.Modified;
        ctx.SaveChanges();
      }
    }

  }
}
