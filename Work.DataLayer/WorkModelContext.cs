﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.Entity;
using Work.Entities;

namespace Work.DataLayer
{
  public class WorkModelContext : DbContext
  {
    public DbSet<Applicant>       Applicants { get; set; }
    public DbSet<EconomySector>   EconomySectors { get; set; }
    public DbSet<EducationRecord> EducationRecords { get; set; }
    public DbSet<Employer>        Employers { get; set; }
    public DbSet<JobRecord>       JobRecords { get; set; }
    public DbSet<Resume>          Resumes { get; set; }
    public DbSet<User>            Users { get; set; }
    public DbSet<Vacancy>         Vacancys { get; set; }

  }
}
