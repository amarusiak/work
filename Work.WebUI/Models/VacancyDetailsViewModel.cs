﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Work.Entities;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Work.WebUI.Models
{
  public class VacancyDetailsViewModel
  {
    [Key, Required]
    public int Id { get; set; }

    //[ForeignKey("Employer")]
    //public int EmployerId { get; set; }         //  FK
    public string EmployerName { get; set; }      //  join

    public string Position { get; set; }
    public string City { get; set; }

    //[ForeignKey("EconomySector")]
    //public int EconomySectorId { get; set; }       //  FK
    public string EconomySectorName { get; set; }      // join

    //[ForeignKey("TypeOfJob")]
    public TypeOfJob TypeOfJobId { get; set; }        //  FK

    public int YearsOfExperience { get; set; }

    //[ForeignKey("TypeOfEducation")]
    public TypeOfEducation TypeOfEducationId { get; set; }  //  FK

    [Required(ErrorMessage = "Please enter a Salary")]
    public int Salary { get; set; }

    public string Description { get; set; }

    //public virtual Employer Employer { get; set; }
    //public virtual EconomySector EconomySector { get; set; }
    //public virtual TypeOfJob TypeOfJob { get; set; }
    //public virtual TypeOfEducation TypeOfEducation { get; set; }

    //public IEnumerable<Vacancy> Vacancys { get; set; }

  }
}