﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Work.Entities;
using Work.Repositories;
using Work.WebUI.Models;

namespace Work.WebUI.Controllers
{
  public class VacancyController : Controller
  {
       private IVacancyRepository _vacancyrepository;
       private IEmployerRepository _employerRepository;
       private IEconomySectorRepository _economySectorRepository;

        #region Constructors
       public VacancyController(
           IVacancyRepository vacancyrepository,
           IEmployerRepository employerRepository,
           IEconomySectorRepository economySectorRepository)
        {
            this._vacancyrepository = vacancyrepository;
            this._employerRepository = employerRepository;
            this._economySectorRepository = economySectorRepository;
        }
        #endregion

    //private EFVacancyRepository _vacancyrepository = new EFVacancyRepository();
    //private SqlVacancyRepository _vacancyrepository = new SqlVacancyRepository();
    //private SqlEmployerRepository _employerRepository = new SqlEmployerRepository();
    //private SqlEconomySectorRepository _economySectorRepository = new SqlEconomySectorRepository();

    // for DI
    //private EFVacancyRepository _efvacancyrepository;
    //public VacancyController(EFVacancyRepository efvacancyrepository)
    //{
    //  this._efvacancyrepository = efvacancyrepository;
    //}

    //public ViewResult List()
    //{
    //  VacancyListViewModel viewModel = new VacancyListViewModel
    //  {
    //    Vacancys = _efvacancyrepository.GetAll()
    //  };
    //  return View(viewModel);
    //}

    // GET: /Vacancy/
    //public ActionResult Index()
    public ViewResult Index()
    {
      VacancyListViewModel viewModel = new VacancyListViewModel
      {
        Vacancys = _vacancyrepository.GetAll()
      };

      //return View();
      //return View(viewModel);
      return View(viewModel.Vacancys);
    }

    // GET: /Vacancy/Details/5
    public ActionResult Details(int id)
    {
      VacancyDetailsViewModel vacancyDetailsViewModel = new VacancyDetailsViewModel();
      Vacancy vacancy = _vacancyrepository.Get(id);

      vacancyDetailsViewModel.Id = vacancy.Id;
      vacancyDetailsViewModel.EmployerName = _employerRepository.Get(vacancy.EmployerId).Name;
      vacancyDetailsViewModel.Position =vacancy.Position;
      vacancyDetailsViewModel.City = vacancy.City;
      vacancyDetailsViewModel.EconomySectorName = _economySectorRepository.Get(vacancy.EconomySectorId).Name;
      vacancyDetailsViewModel.TypeOfJobId = vacancy.TypeOfJobId;
      vacancyDetailsViewModel.YearsOfExperience = vacancy.YearsOfExperience;
      vacancyDetailsViewModel.TypeOfEducationId = vacancy.TypeOfEducationId;
      vacancyDetailsViewModel.Salary = vacancy.Salary;
      vacancyDetailsViewModel.Description = vacancy.Description;

      return View(vacancyDetailsViewModel);
    }

    // GET: /Vacancy/Create
    public ActionResult Create()
    {
      //VacancyDetailsViewModel vacancyDetailsViewModel = new VacancyDetailsViewModel();
      //return View(vacancyDetailsViewModel);

      Vacancy vacancyModel = new Vacancy();
      return View(vacancyModel);
    }

    // POST: /Vacancy/Create
    [HttpPost]
    //public ActionResult Create(FormCollection collection)
    public ActionResult Create(Vacancy vacancy)
    {
      //try
      //{
        // TODO: Add insert logic here

        //if (ModelState.IsValid)
        //{
          this._vacancyrepository.Add(vacancy);
          return RedirectToAction("Index");
        //}
        //else
        //{
        //  return View(vacancy);
        //}
        ////return RedirectToAction("Index");
      //}
      //catch
      //{
      //  return View(vacancy);
      //}
    }

    [HttpPost]  //  !bad
    public ActionResult Save(Vacancy vacancy)
    {
      //if (ModelState.IsValid)
      //{
        this._vacancyrepository.Add(vacancy);
        return RedirectToAction("Index");
      //}

      //  return View(vacancy);
    }


    [HttpPost]  //  ok
    public ActionResult Cancel()
    {
      return RedirectToAction("Index");
    }

    // GET: /Vacancy/Edit/5
    public ActionResult Edit(int id)
    {
      
      return View();
    }

    // POST: /Vacancy/Edit/5
    [HttpPost]
    public ActionResult Edit(int id, FormCollection collection)
    {
      try
      {
        // TODO: Add update logic here

        return RedirectToAction("Index");
      }
      catch
      {
        return View();
      }
    }

    // GET: /Vacancy/Delete/5 - Ok
    [HttpGet]
    public ActionResult Delete(int id)
    {
      try
      {
        // TODO: Add delete logic here
        Vacancy _vacancy = _vacancyrepository.Get(id);
        _vacancyrepository.Remove(_vacancy);

        ViewBag.Vacancies = this._vacancyrepository.GetAll();
        return RedirectToAction("Index");
      }
      catch
      {
        return View("Index");
      }

      //return View();
    }

    // POST: /Vacancy/Delete/5
    [HttpPost]
    public ActionResult Delete(int id, FormCollection collection)
    {
      try
      {
        // TODO: Add delete logic here
        Vacancy _vacancy = _vacancyrepository.Get(id);
        _vacancyrepository.Remove(_vacancy);

        ViewBag.Vacancies = this._vacancyrepository.GetAll();
        return RedirectToAction("Index");
      }
      catch
      {
        return View();
      }
    }
  }
}
