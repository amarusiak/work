﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Work.Entities;
using Work.DataLayer;
using Work.Repositories;
using System.Data.Entity;

namespace ConsoleTest
{
  class Program
  {
    static void Main(string[] args)
    {
      //Database.SetInitializer(
      //  new MigrateDatabaseToLatestVersion<WorkUAModelContext, Configuration>() );

      DBTest1();
    }

    private static void DBTest1()
    {
      // ---
      EFUserRepository efUserRepository = new EFUserRepository();
      for (int i = 1; i <= 10; i++)
      {
        efUserRepository.Add(new User()
        {
          Login = "user" + i.ToString(),
          Password = "password" + i.ToString()
        });
      }

      // ---
      EFApplicantRepository efApplicantRepository = new EFApplicantRepository();
      for (int i = 1; i <= 5; i++)
      {
        efApplicantRepository.Add(new Applicant()
        {
          FirstName = "applFirstName" + i.ToString(),
          LastName = "applLastName" + i.ToString(),
          DateOfBirth = new DateTime(2000, 01, 01),
          City = "paris",
          EMail = "applicant" + i.ToString() + "@hotmail.com",
          UserId = i
        });
      }

      //  ---
      EFEconomySectorRepository efEconomySectorRepository = new EFEconomySectorRepository();
      for (int i = 1; i <= 5; i++)
      {
        efEconomySectorRepository.Add(new EconomySector()
        {
          Name = "economysector" + i.ToString()
        });
      }

      //  ---
      EFResumeRepository efResumeRepository = new EFResumeRepository();
      for (int i = 1; i <= 10; i++)
      {
        efResumeRepository.Add(new Resume()
        {
          ApplicantId = (i % 2 == 0)? 1 : 2,
          //  (int)Math.Floor((double)i / 2),
          EconomySectorId = (i % 2 == 0) ? 1 : 2,
          //  (int)Math.Floor((double)i / 2),
          City = "berlin",
          TypeOfJobId = TypeOfJob.FullTime,
          Salary = 100 * i,
          Description = "resume" + i.ToString()
        });
      }
      //  ---

      EFEducationRecordRepository efEducationRecordRepository = new EFEducationRecordRepository();
      for (int i = 1; i <= 10; i++)
      {
        efEducationRecordRepository.Add(new EducationRecord()
        {
          ResumeId = i,
          TypeOfEducationId = TypeOfEducation.Bachelor,
          College = "cambridge",
          City = "Cambridge",
          FromDate = new DateTime(2000, 2, 2),
          ToDate = new DateTime(2010, 2, 2),
          Description = "cool college"
        });
      }

      // ---
      EFEmployerRepository efEmployerRepository = new EFEmployerRepository();
      //Employer employer1 = new Employer() {Id = 1, Name = "employer1", City = "london", EMail = "employer1@gmail.com", Description = "TNCorp1" };
      for (int i = 1; i <= 5; i++)
      {
        efEmployerRepository.Add(new Employer()
        {
          Name = "employer" + i.ToString(),
          City = "london",
          EMail = "employer" + i.ToString() + "@gmail.com",
          Description = "TNCorp" + i.ToString(),
          UserId = i + 5
        });
      }

      List<Employer> employerList = efEmployerRepository.GetAll();
      foreach (Employer item in employerList)
      {
        Console.WriteLine(item.Name + " " + item.EMail);
      }

      // ---
      EFJobRecordRepository efJobRecordRepository = new EFJobRecordRepository();
      for (int i = 1; i <= 10; i++)
      {
        efJobRecordRepository.Add(new JobRecord()
        {
          ResumeId = i,
          Company = "company" + i.ToString(),
          City = "madrid",
          Position = "developer",
          FromDate = new DateTime(2000, 2, 2),
          ToDate = new DateTime(2010, 2, 2),
          Description = "cool job"
        });
      }

      //  ---
      EFVacancyRepository efVacancyRepository = new EFVacancyRepository();
      for (int i = 1; i <= 10; i++)
      {
        efVacancyRepository.Add(new Vacancy()
        {
          EmployerId = (i % 2 == 0) ? 1 : 2,
          //  (int)Math.Floor((double)i / 2),
          Position = "team lead",
          City = "rome",
          EconomySectorId = (i % 2 == 0) ? 1 : 2,
          //  (int)Math.Floor((double)i / 2),
          TypeOfJobId = TypeOfJob.PartTime,
          YearsOfExperience = 2,
          TypeOfEducationId =TypeOfEducation.Bachelor,
          Salary = 100 * i,
          Description = "cool position"
        });
      }

      //  ---
      Console.ReadLine();
    }
  }


}

