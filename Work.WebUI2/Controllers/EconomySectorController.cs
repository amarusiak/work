﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Work.Entities;
using Work.DataLayer;

namespace Work.WebUI.MVC5.Controllers
{
    public class EconomySectorController : Controller
    {
        private WorkModelContext db = new WorkModelContext();

        // GET: /EconomySector/
        public ActionResult Index()
        {
            return View(db.EconomySectors.ToList());
        }

        // GET: /EconomySector/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EconomySector economysector = db.EconomySectors.Find(id);
            if (economysector == null)
            {
                return HttpNotFound();
            }
            return View(economysector);
        }

        // GET: /EconomySector/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /EconomySector/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,Name")] EconomySector economysector)
        {
            if (ModelState.IsValid)
            {
                db.EconomySectors.Add(economysector);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(economysector);
        }

        // GET: /EconomySector/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EconomySector economysector = db.EconomySectors.Find(id);
            if (economysector == null)
            {
                return HttpNotFound();
            }
            return View(economysector);
        }

        // POST: /EconomySector/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,Name")] EconomySector economysector)
        {
            if (ModelState.IsValid)
            {
                db.Entry(economysector).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(economysector);
        }

        // GET: /EconomySector/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EconomySector economysector = db.EconomySectors.Find(id);
            if (economysector == null)
            {
                return HttpNotFound();
            }
            return View(economysector);
        }

        // POST: /EconomySector/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            EconomySector economysector = db.EconomySectors.Find(id);
            db.EconomySectors.Remove(economysector);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
