﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Work.Entities;
using Work.DataLayer;

namespace Work.WebUI2.Controllers
{
    public class VacancyController : Controller
    {
        private WorkModelContext db = new WorkModelContext();

        //
        // GET: /Vacancy/

        public ActionResult Index()
        {
            var vacancys = db.Vacancys.Include(v => v.Employer).Include(v => v.EconomySector);
            return View(vacancys.ToList());
        }

        //
        // GET: /Vacancy/Details/5

        public ActionResult Details(int id = 0)
        {
            Vacancy vacancy = db.Vacancys.Find(id);
            if (vacancy == null)
            {
                return HttpNotFound();
            }
            return View(vacancy);
        }

        //
        // GET: /Vacancy/Create

        public ActionResult Create()
        {
            ViewBag.EmployerId = new SelectList(db.Employers, "Id", "Name");
            ViewBag.EconomySectorId = new SelectList(db.EconomySectors, "Id", "Name");
            return View();
        }

        //
        // POST: /Vacancy/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Vacancy vacancy)
        {
            if (ModelState.IsValid)
            {
                db.Vacancys.Add(vacancy);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.EmployerId = new SelectList(db.Employers, "Id", "Name", vacancy.EmployerId);
            ViewBag.EconomySectorId = new SelectList(db.EconomySectors, "Id", "Name", vacancy.EconomySectorId);
            return View(vacancy);
        }

        //
        // GET: /Vacancy/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Vacancy vacancy = db.Vacancys.Find(id);
            if (vacancy == null)
            {
                return HttpNotFound();
            }
            ViewBag.EmployerId = new SelectList(db.Employers, "Id", "Name", vacancy.EmployerId);
            ViewBag.EconomySectorId = new SelectList(db.EconomySectors, "Id", "Name", vacancy.EconomySectorId);
            return View(vacancy);
        }

        //
        // POST: /Vacancy/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Vacancy vacancy)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vacancy).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.EmployerId = new SelectList(db.Employers, "Id", "Name", vacancy.EmployerId);
            ViewBag.EconomySectorId = new SelectList(db.EconomySectors, "Id", "Name", vacancy.EconomySectorId);
            return View(vacancy);
        }

        //
        // GET: /Vacancy/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Vacancy vacancy = db.Vacancys.Find(id);
            if (vacancy == null)
            {
                return HttpNotFound();
            }
            return View(vacancy);
        }

        //
        // POST: /Vacancy/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Vacancy vacancy = db.Vacancys.Find(id);
            db.Vacancys.Remove(vacancy);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}