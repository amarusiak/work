using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc4;

using Work.Repositories;

namespace Work.WebUI2
{
  public static class Bootstrapper
  {
    public static IUnityContainer Initialise()
    {
      var container = BuildUnityContainer();

      DependencyResolver.SetResolver(new UnityDependencyResolver(container));

      return container;
    }

    private static IUnityContainer BuildUnityContainer()
    {
      var container = new UnityContainer();

      // register all your components with the container here
      // it is NOT necessary to register your controllers

      // e.g. container.RegisterType<ITestService, TestService>();    
      RegisterTypes(container);

      return container;
    }

    public static void RegisterTypes(IUnityContainer container)
    {
      //container.RegisterType<IApplicantRepository, SqlApplicantRepository>();
      //container.RegisterType<IEconomySectorRepository, SqlEconomySectorRepository>();
      //container.RegisterType<IEducationRecordRepository, SqlEducationRecordRepository>();
      //container.RegisterType<IEmployerRepository, SqlEmployerRepository>();
      //container.RegisterType<IJobRecordRepository, SqlJobRecordRepository>();
      //container.RegisterType<IResumeRepository, SqlResumeRepository>();
      //container.RegisterType<IVacancyRepository, SqlVacancyRepository>();

      container.RegisterType<IApplicantRepository, EFApplicantRepository>();
      container.RegisterType<IEconomySectorRepository, EFEconomySectorRepository>();
      container.RegisterType<IEducationRecordRepository, EFEducationRecordRepository>();
      container.RegisterType<IEmployerRepository, EFEmployerRepository>();
      container.RegisterType<IJobRecordRepository, EFJobRecordRepository>();
      container.RegisterType<IResumeRepository, EFResumeRepository>();
      container.RegisterType<IUserRepository, EFUserRepository>();
      container.RegisterType<IVacancyRepository, EFVacancyRepository>();

    }
  }
}